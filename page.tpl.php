<?php
?><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="<?php print $language->language ?>" xml:lang="<?php print $language->language ?>" dir="<?php print $language->dir ?>">

<head>
  <title><?php print $head_title ?></title>
  <?php print $head ?>
  <?php print $styles ?>
  <?php print $scripts ?>
  <script type="text/javascript"><?php /* Needed to avoid Flash of Unstyle Content in IE */ ?> </script>
</head>

<body>
<div id="maintopPan">
  <div id="maintopPanOne">
   <div id="topHeaderPan">
   		<?php if ($logo) { ?><a href="<?php print $front_page ?>" title="<?php print t('Home') ?>"><img src="<?php print $logo ?>" alt="<?php print t('Home') ?>" /></a><?php } ?>
			</div>
		<div id="topSidemenuPan">
		<ul>
			<li class="home"><a href="#">Home</a></li>
			<li class="contact"><a href="#">Contact</a></li>
		</ul>
		</div>
  </div>
    
</div>

<div id="bodyPan">
  <div id="leftPan">
<div class="primmenu"><?php if (isset($primary_links)) { ?><?php print theme('links', $primary_links, array('class' => 'links', 'id' => 'navlist')) ?><?php } ?></div>
	<?php if ($mission) { ?><div id="mission"><?php print $mission ?></div><?php } ?>
      <div id="main">
        <?php print $breadcrumb ?>
        <h1 class="title"><?php print $title ?></h1>
        <div class="tabs"><?php print $tabs ?></div>
        <?php if ($show_messages) { print $messages; } ?>
        <?php print $help ?>
        <?php print $content; ?>
        <?php print $feed_icons; ?>
  </div>
</div>  
  <div id="rightPan">
	<div id="rightbottomPan">
  	<?php print $left ?>
  </div>
  </div>
</div>
<div id="footermainPan">
  <div id="footerPan">
<?php if (isset($primary_links)) { ?><?php print theme('links', $primary_links, array('class' => 'links', 'id' => 'subnavlist')) ?><?php } ?>
 <div class="footerlink">
<div class="designby"><ul><li>Design By - <a href="http://www.templateworld.com">Template World</a></li>
</ul></div>
<div class="designby3"><ul><li>Drupal Theme By - <a href="http://www.neerjasoftwares.com">Neerjasoftware</a></li>
</ul></div>
<p class="copyright"><?php print $footer_message ?></p>
</div>  
  </div>
 </div>
<?php print $closure ?>
</body>
</html>
